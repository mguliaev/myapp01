package com.example.mike5.myapp01;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class NotificationFragment extends BaseFragment {

    public static NotificationFragment newInstance(int fragmentNum) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putInt("FragmentNum", fragmentNum);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            FragmentNum = getArguments().getInt("FragmentNum");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.notification_fragment, null);
        Button buttonNext = (Button) v.findViewById(R.id.buttonNext);
        final TextView textViewCount = (TextView) v.findViewById(R.id.textViewCount);
        textViewCount.setText(Integer.toString(FragmentNum));
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment fragment = SettingsFragment.newInstance(FragmentNum + 1);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragmentBase, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return v;
    }

}
