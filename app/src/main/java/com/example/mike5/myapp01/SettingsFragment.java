package com.example.mike5.myapp01;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.plus.PlusOneButton;

public class SettingsFragment extends BaseFragment {

    public static SettingsFragment newInstance(int fragmentNum) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt("FragmentNum", fragmentNum);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            FragmentNum = getArguments().getInt("FragmentNum");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.settings_fragment, null);
        Button buttonNext = (Button) v.findViewById(R.id.buttonNext);
        final TextView textViewCount = (TextView) v.findViewById(R.id.textViewCount);
        textViewCount.setText(Integer.toString(FragmentNum));
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                Fragment fragment = ProductsFragment.newInstance(FragmentNum + 1);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragmentBase, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return v;
    }
}
