package com.example.mike5.myapp01;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    private int FragmentNum = -1;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigation.inflateMenu(R.menu.bottom_menu);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.menuProducts:
                        FragmentNum = 1;
                        fragment = ProductsFragment.newInstance(FragmentNum);
                        break;
                    case R.id.menuNotification:
                        FragmentNum = 1;
                        fragment = NotificationFragment.newInstance(FragmentNum);
                        break;
                    case R.id.menuSettings:
                        FragmentNum = 1;
                        fragment = SettingsFragment.newInstance(FragmentNum);
                        break;
                }
                if (fragment != null) {
                    Clear_BackStack();
                    final FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragmentBase, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("FragmentNum", FragmentNum);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FragmentNum = savedInstanceState.getInt("FragmentNum");
    }

    void Clear_BackStack() {
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        if(NeedFinish()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    boolean NeedFinish() {
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() <= 1) {
            return true;
        }
        return false;
    }

}
